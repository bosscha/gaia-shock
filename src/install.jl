## install package for Gaia.Clustering

using Pkg

Pkg.add("PyCall")
Pkg.add("DataFrames")
Pkg.add("Clustering")
Pkg.add("Statistics")
Pkg.add("Distributions")
Pkg.add("Random")
Pkg.add("CSV")
Pkg.add("PyPlot")
Pkg.add("Formatting")

println("## Package installation for GaiaClustering done...")
