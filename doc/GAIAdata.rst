************
GAIA data
************

This section explains how to load and tomanipulate the GAIA data from a source name or sky coordinates


Server
============



Format
========



Use
====

Module: **gaia_utils**

.. autoclass:: source

.. code-block:: python
  :caption: Example with gaia-utils and class source
  
   import gaia_utils as gU

   s = gU.source("NGC 2516")  # instantiate the class source with an object name



Module: *gaia_utils*

.. autoclass:: source
   
   

