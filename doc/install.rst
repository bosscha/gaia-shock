************
Installation
************

Requirements
============

The python modules need the following external modules.

* matplotlib
* astropy
* astroquery
* scikit-learn (clustering)


The jupyter can be installed using 
pip3 install jupyter
