.. GAIA-shock documentation master file, created by
   sphinx-quickstart on Fri Aug 24 15:23:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GAIA-shock's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   install.rst
   sample.rst
   GAIAdata
   clustering
   analysis


Scope of the project
====================

The project is meant to provide the scripts necessary to deal with the stellar cluster data read from GAIA archive and the analysis methods.
The scripts are mainly written in python using external libraries like numpy, astropy, scikit-learn and in Julia using various modules (Clusterings, Stats, etc)


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
